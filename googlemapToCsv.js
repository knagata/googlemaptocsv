var map;
var service;
var infowindow;
var target = new google.maps.LatLng(35.6583247,139.7029853);
var tokyo = new google.maps.LatLng(35.681382,139.766084);
var width;
var height;
var type;
var items;
var names;
var url;

function initialize() {
  //init properties
  loc = document.form.target.value;
  width = document.form.width.value;
  height = document.form.height.value;
  type = document.form.type.value;
  kw = document.form.keyword.value;
  method = document.form.method.value;
  items = []
  names = []

  //init google places api
  map = new google.maps.Map(document.getElementById('map-canvas'), {
    center: target, zoom: 15,
    styles: [
      { stylers: [ { visibility: 'simplified' } ] },
      { elementType: 'labels',stylers: [ { visibility: 'off' } ] }
    ]
  });
  service = new google.maps.places.PlacesService(map);
  infowindow = new google.maps.InfoWindow();

  //init target area and view
  var t = {location: tokyo,radius: 10000, keyword: loc};
  service.nearbySearch(t, setTarget);

  function setTarget(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
      if(results.length>0){target = results[0].geometry.location;}
      bound = ToLatlngBounds(target, width, height, "center");
    }else{
      window.alert("failed");
    }
  };

  //find items
  findItems(target,width,height,type);
};

function findItems(centerLatLng, w, h, type){
  var latOffset = h * 180 / 40075016.6856;
  var lngOffset = w * 180 / 40075016.6856;
  var edge = new google.maps.LatLng(centerLatLng.lat()-latOffset,centerLatLng.lng()-lngOffset);

  wDiv = Math.round(w/500);
  hDiv = Math.round(h/500);
  sw = w/wDiv;
  sh = h/hDiv;
  slat = sh * 360 / 40075016.6856;
  slng = sw * 360 / 40075016.6856;

  var time = hDiv*wDiv;

  window.alert("ターゲットの取得には、推定 "+time+" 秒かかります");

  var bound = ToLatlngBounds(edge,w,h,"corner");
  var rectangle = new google.maps.Rectangle({
    strokeColor: '#000000',
    strokeOpacity: 1,
    strokeWeight: 1.5,
    fillColor: '#ffffff',
    fillOpacity: 0,
    map: map,
    bounds: bound
  });

  for(var i=0; i<hDiv; i++){
    for(var j=0; j<wDiv; j++){
      var corner = new google.maps.LatLng(edge.lat()+slat*i,edge.lng()+slng*j);
      var bound = ToLatlngBounds(corner,sw,sh,"corner");
      var rectangle = new google.maps.Rectangle({
        strokeColor: '#000000',
        strokeOpacity: 0.3,
        strokeWeight: 0.5,
        fillColor: '#ffffff',
        fillOpacity: 0.05,
        map: map,
        bounds: bound
      });
      if(method == "t"){
        var request = {bounds: bound, types: [type] };
      }else{
        var request = {bounds: bound, keyword: kw };
      }
      //used to be radar
      setTimeout(service.nearbySearch(request, find),800);
    }
  }
  
  setTimeout(
    function createMarkers(){
      for(var i=0; i<items.length; i++){
        createMarker(items[i]);
        names[i] = items[i].name;
      }
      //console.error(names);
      window.alert("ターゲットを "+items.length+" 箇所取得しました。");
    },
    time*1000
  );

  function find(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
      for (var i = 0; i < results.length; i++) {
        items[items.length] = results[i];
      }
    }
  };
}

function generate(){
  if (!window.File) {
    window.alert("今どきFile APIが使えないブラウザ使ってるとかホントありえない、、、");
  }else{
    var texts = new Array(items.length);

    window.alert("ファイル生成には、推定 "+ items.length +" 秒かかります")

    setTimeout(function(){
      for(var i=0;i<items.length;i++){
        console.error(names[i])
        var text = "";
        text += i+","
        text += names[i] + ",";
        text += items[i].geometry.location.lat() + ",";
        text += items[i].geometry.location.lng();
        text += "\n";
        texts[i] = text;
      }

      var blob = new Blob(texts, {type: "csv"});
      var url = window.URL.createObjectURL(blob);

      document.getElementById("download").href = url;
      document.getElementById("download").download = "result.csv";

      window.alert("ファイル生成を終了しました")
    },500);
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location,
    icon: {
      url: 'http://maps.gstatic.com/mapfiles/circle.png',
      anchor: new google.maps.Point(10, 10),
      scaledSize: new google.maps.Size(10, 17)
    }
  });

  google.maps.event.addListener(marker, 'click', function() {
    service.getDetails(place, function(res, status) {
      if (status !== google.maps.places.PlacesServiceStatus.OK) {
        console.error(status);
        return;
      }
      infowindow.setContent(res.name);
      infowindow.open(map, marker);
    });
  });
  return place.name;
}

function ToLatlngBounds(latlng, w, h, type){
  latBnd = h * 360 / 40075016.6856;
  lngBnd = w * 360 / 40075016.6856;

  if(type == "corner"){
    bound = new google.maps.LatLngBounds(
      new google.maps.LatLng(latlng.lat(), latlng.lng()),
      new google.maps.LatLng(latlng.lat()+latBnd, latlng.lng()+lngBnd)
    );
  }else if(type == "center"){
    bound = new google.maps.LatLngBounds(
      new google.maps.LatLng(latlng.lat()-latBnd/2, latlng.lng()-lngBnd/2),
      new google.maps.LatLng(latlng.lat()+latBnd/2, latlng.lng()+lngBnd/2)
    );
  }
  return bound;
}

function Sleep( milli_second )
{
    var start = new Date();
    while( new Date() - start < milli_second );
}

google.maps.event.addDomListener(window, 'load', initialize);